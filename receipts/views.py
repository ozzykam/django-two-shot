from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


@login_required
def show_receipt(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    receipts.purchaser = request.user

    context = {
        "receipts": receipts,
        "purchaser": receipts.purchaser,
    }

    return render(request, "receipts/list.html", context)
    # returns all instances

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    
    context ={
        "form": form
    }
    return render(request, "receipts/create.html", context)

@login_required
# def category_list(request):
#     category_filter = ExpenseCategory.objects.filter(owner=request.user)
    
#     categories =[]

#     for category in category_filter:
#         total = Receipt.objects.filter(purchaser=request.user, category=category).count()
#         categories.append(
#             {
#                 "category":category, 
#                 "total":total,
#             }
#         )

#     context = {
#         "categories":categories,
#     }
#     return render(request, "receipts/categories.html", context)
def category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "receipts/categories.html", context)


@login_required
# def account_list(request):
#     bank_filter = Account.objects.filter(owner=request.user)

#     banks = []

#     for account in bank_filter:
#         total = Receipt.objects.filter(purchaser=request.user, account=account).count()
#         banks.append(
#             {
#                 'account':account,
#                 'total':total,
#             }
#         )
#     context = {
#         "banks":banks
#     }
    
#     return render(request, "receipts/accounts.html", context)
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/accounts.html", context)



@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()

    context = {
        "form":form,
    }
    return render(request, "receipts/create_category.html", context)

@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()
    
    context = {
        "form": form
    }
    return render(request, "receipts/create_account.html", context)